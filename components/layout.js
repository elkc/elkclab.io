import * as React from "react";
import Head from "next/head";
import Link from "next/link";

const styles = {
  navItem: {
    padding: 10,
    transition: "200ms all",
    cursor: "pointer"
  }
};

export const Layout = ({ children, title, description }) => {
  const renderNavItems = () => {
    return [
      { path: "/", name: "Home" },
      { path: "/about", name: "About" },
      { path: "/news", name: "News" },
      { path: "/supregs", name: "Supregs" },
      { path: "/downloads", name: "Downloads" },
      { path: "/contact", name: "Contact" }
    ].map(({ path, name }) => (
      <div key={path} style={styles.navItem}>
        <Link href={path}>
          <a style={{ color: "white", textDecoration: "none" }}>{name}</a>
        </Link>
      </div>
    ));
  };
  return (
    <div>
      <Head>
        <meta name="viewport" content="width=device-width" />
        <title>{title + " - Eastern Lions Kart Club"}</title>
        <meta
          name="description"
          content={
            description ||
            "The Eastern Lions Kart Club was established in 1961, located as a track at Humevale near Whittlesea. The memberhip consists of approximately 160 karters and their families and is drawn from predominantly the Northern and Northern Eastern Suburbs of Melbourne and a growing regional membership."
          }
        />
        <link rel="icon" href="/static/img/helmet.png" type="image/png" />
      </Head>
      <style global jsx>{`
        @import url("https://fonts.googleapis.com/css?family=Titillium+Web:400,700");
        * {
          box-sizing: border-box;
        }
        body {
          margin: 0;
          background: #ededed;
          font-family: "Titillium Web", sans-serif;
        }
        h1 {
          text-align: center;
          font-size: 1.8em;
          font-weight: bold;
          text-transform: uppercase;
        }
        .page img {
          width: 100%;
        }
        nav > div:hover a {
          color: #fd1a0a;
        }
        table {
          word-break: break-all;
          width: 100%;
        }
      `}</style>
      <div
        style={{
          width: "100%",
          maxWidth: 930,
          margin: "0 auto",
          background: "white",
          overflowX: "scroll"
        }}
      >
        <header>
          <img
            src="/static/img/header.jpg"
            style={{ display: "block", width: "100%" }}
          />
          <nav
            style={{
              display: "flex",
              flexFlow: "row wrap",
              justifyContent: "center",
              textTransform: "uppercase",
              background: "black"
            }}
          >
            {renderNavItems()}
          </nav>
        </header>
        <div
          className="page"
          style={{
            width: "100%",
            maxWidth: 800,
            margin: "0 auto",
            padding: 20
          }}
        >
          {children}
        </div>
      </div>
    </div>
  );
};

export default Layout;
