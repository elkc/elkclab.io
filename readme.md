# How to

## Update a page

In `pages/content/` you will find markdown files for each page. To make formatting
changes, use this markdown cheatsheet:

https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

## To add a new file link to a page

Upload the file in `static/` to the appropriate folder. Remember/copy the path 
and file name so you can add the link to the page content you want.

eg. [April 2018 Supp Regs](/static/suppregs/April_2018_supp_regs.docx) will link
to a supreg document.

## To add a new page

Add a new JS file under `pages/` that exports a React Element. For markdown 
content, also add a corresponding `.md` file in `pages/content/` and require it
in the React element using `ReactMarkdown`.

## To add a new page link to the nav bar

Open `components/layout.js` and add a new item to the nav list. 
eg. `{ path: "/contact", name: "Contact" }`