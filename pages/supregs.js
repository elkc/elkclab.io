import ReactMarkdown from "react-markdown";
import Layout from "../components/layout";

export default () => (
  <Layout title="Supregs">
    <ReactMarkdown source={require("./content/supregs.md")} />
  </Layout>
);
