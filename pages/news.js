import ReactMarkdown from "react-markdown";
import Layout from "../components/layout";

export default () => (
  <Layout title="News">
    <ReactMarkdown source={require("./content/news.md")} />{" "}
  </Layout>
);
