import ReactMarkdown from "react-markdown";
import Layout from "../components/layout";

export default () => (
  <Layout title="About">
    <ReactMarkdown source={require("./content/about.md")} />,
  </Layout>
);
