import ReactMarkdown from "react-markdown";
import Layout from "../components/layout";

export default () => (
  <Layout title="Home">
    <ReactMarkdown source={require("./content/home.md")} />
  </Layout>
);
