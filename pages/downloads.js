import ReactMarkdown from "react-markdown";
import Layout from "../components/layout";

export default () => (
  <Layout title="Downloads">
    <ReactMarkdown source={require("./content/downloads.md")} />
  </Layout>
);
