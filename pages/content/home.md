Karting is the world’s most affordable form of motorsport.

Many people associate it with young drivers, but adults are also very active in karting.
Karting is considered the first step in any serious racer's career. It can prepare the driver for high-speed wheel-to-wheel racing by developing quick reflexes, precision, car control, set-up, mechanical and decision-making skills.

People of all ages can enjoy the sport with children as young as seven competing behind the wheel. Karting teaches these children, both boys and girls, basic driving skills well before they are able to obtain their road licence.

Karting is competitive, but it is also fun and family orientated, with involvement in the sport both on and off the track giving it a strong feeling of community. It teaches driving skills and offers people an opportunity to gain a career in motorsport, all in a safe and organised environment.

Kart meetings take place at circuits across Australia every weekend and are either a club, regional, state or national level meeting.
Getting into karting is relatively simple. First you need to become a member of your local kart club and start out on a provisional licence. Buying a kart has also been made a lot simpler thanks to a professional trade industry that serves the karters of Australia.

![karting](static/img/Go-Karting.jpg)
