# Contact

# Eastern Lions Kart Club

## Postal Address

PO Box 11
Montrose, 3765
A.B.N. 31 960 235 541

**Contact Details**
Mobile - 0418 511 140
Other Mobile - 0438 570 755
Email - general@elkc.com.au

Position                  | Name                | Email                    | Phone          
------------------------- | ------------------- | ------------------------ | -----------
**President**             | Steve Pegg          | president@elkc.com.au    | 0438 570 755
**Vice President**        | Justin Paragreen    | vp@elkc.com.au           | 0434 312 777
**Secretary**             | Ross Harrod         | secretary@elkc.com.au    | 0418 511 140
**Treasurer**             | Warren Johns        | treasurer@elkc.com.au    | 0419 518 275
**Membership Secretary**  | Ian Branson         | membsec@elkc.com.au      | 0419 882 132
**Race Entry**            | Kelli Richardson    | raceentry@elkc.com.au    | 0466 260 916
**VKA Delegate**          | Brett Hughes        | vkadelegates@elkc.com.au | 0414 912 988
**VKA Delegate**          | Justin Paragreen    | vkadelegates@elkc.com.au | 0434 312 777
**Website**               | David Thomson       | website@elkc.com.au      | 0419 208 771
**Trophies**              | Glen Chadwick       | trophies@elkc.com.au     | 0418 347 512
**Race Committee**        | Ross Harrod         | racecommittee@elkc.com.au| 0418 511 140
****                      | Sarge Wilson        |                          |
**Race Committee**        | Brendon Myors       |                          |
**Race Committee**        | Ian Branson         |                          |
**Race Committee**        | Gary Heywood        |                          |
**Race Committee**        | Justtin Paragreen   |                          |
**Track & Grounds**       | Michael O'Brien     | track@elkc.com.au        | 0410 527 076
**Canteen**               | Michael & Julie O'Brien|canteen@elkc.com.au     | 0410 527 076

![ELKC Map](/static/img/elkc_map.gif)