import ReactMarkdown from "react-markdown";
import Layout from "../components/layout";

export default () => (
  <Layout title="Contact">
    <ReactMarkdown source={require("./content/contact.md")} />
  </Layout>
);
