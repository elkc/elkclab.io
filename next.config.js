const glob = require("glob");

module.exports = {
  webpack: (config, { buildId, dev, isServer, defaultLoaders }) => {
    config = {
      ...config,
      module: {
        ...config.module,
        rules: [
          ...config.module.rules,
          {
            test: /\.md$/,
            use: "raw-loader"
          }
        ]
      }
    };
    return config;
  },
  exportPathMap: function(defaultPathMap) {
    let pages = glob.sync("./pages/*.js");
    let pathMap = {};
    pages.forEach(file => {
      let name = file
        .split("/")
        .pop()
        .split(".")[0];

      if (name === "index") {
        pathMap["/"] = { page: "/index" };
      } else {
        pathMap["/" + name] = { page: "/" + name };
      }
    });
    return pathMap;
  }
};
